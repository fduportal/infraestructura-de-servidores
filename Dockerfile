FROM debian:buster

ENV USER_FTP _
ENV PASS_FTP _

COPY entrypoint.sh /
COPY run_ftp.sh /  

EXPOSE 22

ENTRYPOINT ["/entrypoint.sh"]

CMD sh /run_ftp.sh && tail -f /dev/null
