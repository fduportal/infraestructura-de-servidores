# TP Final

Fecha: 07/12/2021

Nombre y apellido: Federico Duportal

Materia: Infraestructura de Servidores

Profesor: Sergio Pernas



### Resumen

A continuación veremos como construir una imagen docker y su posterior deploy para levantar un servidor ftp.

Esta aplicación contendrá los siguientes archivos:

- Dockerfile

- entrypoint.sh

- run_ftp.sh

  

**Requisitos:**

- Docker

- Sistema Operativo Ubuntu

  

**Instalación de Docker**

- Se instalan paquetes y dependencias
- Se importan llaves
- Se agregar repositorio
- Se instala Docker

~~~
# apt update && apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
# curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add-
# add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
# apt update
# apt install docker-ce docker-ce-cli containerd.io
~~~



**Fichero Dockerfile**

En este fichero encontramos las instrucciones para armar la imagen, en donde encontraremos

- FROM: Imagen base para la creación de la imagen
- ENV: Las variables de entorno que recibirá al momento de ejecutar el contenedor. En este caso el usuario y contraseña
- Se copian los script **entrypoint.sh** y **run_ftp.sh** a la raíz del contenedor para su posterior ejecución
- Se expone el puerto 21 (FTP)
- Se llama al script **entrypoint.sh** que será el encargado de realizar toda la instalación y configuración del servicio
- Se ejecuta el script **run_ftp.sh** para levantar el servicio y se agregar **tail -f /dev/null** para mantener la ejecución del contenedor

~~~
FROM debian:buster

ENV USER_FTP _
ENV PASS_FTP _

COPY entrypoint.sh /
COPY run_ftp.sh /  

EXPOSE 21

ENTRYPOINT ["/entrypoint.sh"]

CMD sh /run_ftp.sh && tail -f /dev/null
~~~



**Script entrypoint.sh**

Este script será el encargado de realizar la instalación y configuración del servicio sftp

- Se actualizan respositorios e instala servicio vsftp
- Se crea la carpeta **/var/ftp** que se utilizará para subir los archivos del usuario
- Se agrega el usuario **$USER_FTP** al grupo **ftp** 
- Se dan permisos sobre la carpeta **/var/ftp**
- Se habilita **write_enable** para obtener permiso de escritura sobre la carpeta FTP

~~~
#!/bin/bash

set -e

apt-get update && apt-get install vsftpd -y

mkdir /var/ftp

useradd -g ftp -d /var/ftp $USER_FTP

chmod -R 777 /var/ftp

echo $USER_FTP:$PASS_FTP | chpasswd

sed -i 's/#write_enable=YES/write_enable=YES/' /etc/vsftpd.conf

exec "$@"
~~~



**Script run_ftp.sh**

Este script sirve únicamente para levantar el servicio de **ftp** una vez finalice su instalación y configuración

~~~
#!/bin/bash

service vsftpd start
~~~



**Creación de la imagen**

~~~
# docker build -t ftp .
~~~



**Ejecución del contenedor**

Se ejecuta contenedor, pasandole como parámetro

- -d: Ejecución en segundo plano del contenedor
- -p 21:21: Forwarding del puerto 21 del host al 21 del contenedor
- -e USER_FTP="usuario" -e PASS_FTP="password": Se pasan como parámetros el usuario y contraseña
- --name ftp: Nombre del contenedor
- ftp: Nombre de la imagen a utilizar

~~~
# docker run -d -p 21:21 -e USER_FTP='usuario' -e PASS_FTP='password' --name ftp ftp
~~~







