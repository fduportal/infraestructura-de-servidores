#!/bin/bash

set -e

apt-get update && apt-get install vsftpd -y

mkdir /var/ftp

useradd -g ftp -d /var/ftp $USER_FTP 

chmod -R 777 /var/ftp

echo $USER_FTP:$PASS_FTP | chpasswd

sed -i 's/#write_enable=YES/write_enable=YES/' /etc/vsftpd.conf

exec "$@"
